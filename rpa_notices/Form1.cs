﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using rpa_notices.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace rpa_notices
{
    public partial class Rpa_notice_Sesc : Form
    {
        public Rpa_notice_Sesc()
        {
            InitializeComponent();

            //Retirando a opção de redimensionamento do usuario
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Abrir navegor Chrome
            IWebDriver driver = new ChromeDriver(Environment.CurrentDirectory);

            try
            {
                //Acessa o site
                driver.Navigate().GoToUrl("https://www.sescma.com.br/");

                //Maximiza tela
                driver.Manage().Window.Maximize();



                //Scrapping da pag
                var listaNoticias = driver
                    .FindElement(By.Id("ultimas-noticias"))
                    .FindElement(By.ClassName("nav"))
                    .FindElements(By.TagName("li"));


                var lista = new List<LastNotices>();

                foreach (var item in listaNoticias)
                {
                    var noticia = new LastNotices();



                    noticia.Sobre = item.FindElement(By.CssSelector("li > a")).Text;
                    noticia.Link = item.FindElement(By.CssSelector("li> a")).GetProperty("href");
                    noticia.Data = item.FindElement(By.CssSelector("li > span")).Text;
                    lista.Add(noticia);

                }

                //dataGridView1.Columns[0].HeaderText = "Data";
                //dataGridView1.Columns[1].HeaderText = "Sobre";
                //dataGridView1.Columns[2].HeaderText = "Link";


                driver.Quit();
                //  dataGridView1.Enabled = true;
                for (int i = 0; i < lista.Count; i++)
                {

                    dataGridView1.Rows.Add();

                    dataGridView1.Rows[i].Cells[0].Value = lista.ElementAt(i).Data;
                    dataGridView1.Rows[i].Cells[1].Value = lista.ElementAt(i).Link;
                    dataGridView1.Rows[i].Cells[2].Value = lista.ElementAt(i).Sobre;





                }
            }
            catch (Exception)
            {

                driver.Quit();

                MessageBox.Show("Ocorreu um erro inesperado, talvez a versão do site tenha sido atualizada");
            }



        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            //dataGridView1.Enabled = false;

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
